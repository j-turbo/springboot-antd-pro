package com.hello123.model.demo;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @since 2020/01/27 23:08:04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "t_user")
public class User implements Serializable {

	private static final long serialVersionUID = -4539611541616691884L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Transient
	private Long rowKey;
	
	private String name;
	
	private String type;
	
	private Byte sex;
	
	private Date birthday;
	
	private Integer age;
	
	@Column(name="note")
	private String desc;
	
	public Long getRowKey() {
		return this.id;
	}
}
