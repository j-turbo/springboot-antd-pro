package javax.xianfeng.jwt.api;

import java.util.Date;

import javax.xianfeng.jwt.model.JwtUser;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

/**
 * @since 2020/01/29 17:09:46
 */
public final class JwtApi {
	
	private JwtApi() {
		super();
	}

	public static String getToken(JwtUser user) {
		return JWT.create().withAudience(user.getId()).sign(Algorithm.HMAC256(user.getPassword()));
	}

	public static String getToken(JwtUser user, Date expiredTime) {
		return JWT.create().withExpiresAt(expiredTime).withAudience(user.getId()).sign(Algorithm.HMAC256(user.getPassword()));
	}

	
}
